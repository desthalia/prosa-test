# prosa-test

Technical test for NLP AI Engineer position at Prosa.

Semua proses eksperimen serta demo menggunakan modelnya ada di file model.ipynb.

Libraries used:
- Pandas
- Numpy
- Matplotlib
- Spacy
- Gensim
- PyTorch